import os
import random
import pygame
import neat

pygame.init()

WIDTH, HEIGHT = 700, 500
WIN = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Flappy AI")
FONT = pygame.font.Font('freesansbold.ttf', 32)

BLUE = (70, 156, 232)
WHITE = (255, 255, 255)
GREEN = (57, 252, 3)

PLAYER_RADIUS = 20
JUMP_VEL = 4.6
GRAVITY_VEL = 0.25

PIPE_VEL = 5
PIPE_GAP = PLAYER_RADIUS * 6
PIPE_WIDTH = PLAYER_RADIUS * 3
PIPE_HEIGHT = HEIGHT
PIPE_START = WIDTH
PIPE_END = -PIPE_WIDTH
PIPE_MIN_Y = HEIGHT/20 - HEIGHT
PIPE_MAX_Y = -(HEIGHT/20) - PIPE_GAP

FPS = 60


class Player:
    def __init__(self, x, y, color):
        self.x = x
        self.y = y
        self.color = color
        self.speed = 0

    def jump(self):
        self.speed = -JUMP_VEL

    def update(self):
        self.speed += GRAVITY_VEL
        self.y += self.speed

    def collided(self, pipe):
        if self.y - PLAYER_RADIUS <= 0 or self.y + PLAYER_RADIUS >= HEIGHT:
            return True, "world"
        if (self.x + PLAYER_RADIUS >= pipe.x and self.x - PLAYER_RADIUS <= pipe.x + PIPE_WIDTH) \
                and (self.y - PLAYER_RADIUS <= pipe.y + PIPE_HEIGHT or self.y + PLAYER_RADIUS >= pipe.y + PIPE_HEIGHT + PIPE_GAP):
            return True, "pipe"

        return False, None

    def passed(self, pipe):
        if self.x - PLAYER_RADIUS >= pipe.x + PIPE_WIDTH:
            #print("Passed pipe")
            return True

        return False

    def draw(self):
        pygame.draw.circle(WIN, self.color, (self.x, self.y), PLAYER_RADIUS)


class Pipe:
    def __init__(self, x, y, color):
        self.x = x
        self.y = y
        self.isOut = False
        self.color = color

    def update(self):
        if self.x == PIPE_END:
            #self.x = PIPE_START
            #self.y = random.randint(PIPE_MIN_Y, PIPE_MAX_Y)
            self.isOut = True
        else:
            self.x -= PIPE_VEL

    def draw(self):
        pygame.draw.rect(WIN, self.color, pygame.Rect(self.x, self.y, PIPE_WIDTH, PIPE_HEIGHT))
        pygame.draw.rect(WIN, self.color, pygame.Rect(self.x, self.y + PIPE_HEIGHT + PIPE_GAP, PIPE_WIDTH, PIPE_HEIGHT))


def draw_widow(players, pipes, score, alive, GEN):
    WIN.fill(BLUE)
    for player in players:
        player.draw()
    for pipe in pipes:
        pipe.draw()

    score_text = FONT.render(f"Score: {score}", True, WHITE)
    score_rect = score_text.get_rect()
    WIN.blit(score_text, (WIDTH - score_rect.width, 0))

    alive_text = FONT.render(f"Alive: {alive}", True, WHITE)
    alive_rect = alive_text.get_rect()
    WIN.blit(alive_text, (0, 0))

    gen_text = FONT.render(f"Gen: {GEN}", True, WHITE)
    WIN.blit(gen_text, (0, alive_rect.height))

    pygame.display.update()

def get_random_color():
    valid = False
    while not valid:
        color = (random.randint(0, 255),
                 random.randint(0, 255),
                 random.randint(0, 255))
        if color != GREEN and color != BLUE:
            valid = True

    return color

FIT_ADVANCE = 0.05
FIT_PASS_PIPE = 5
FIT_COLLIDE = -5
GEN = 0

# main game loop here
# takes all the gnomes(players/attempts) and evaluates them (they are neural networks)
def main(genomes, config):
    global GEN
    GEN += 1
    # keep track of neural network and player associeted, (indexes mache)
    nets = []
    ge = []
    players = []

    # genome is a tupple of 2 elements with the first value as an id: we dont wanna use it
    for _, g in genomes:
        # feed forward: https://stackoverflow.com/questions/28403782/what-is-the-difference-between-back-propagation-and-feed-forward-neural-network
        net = neat.nn.FeedForwardNetwork.create(g, config)
        nets.append(net)
        players.append(Player(WIDTH / 2, HEIGHT / 2, get_random_color()))
        g.fitness = 0
        ge.append(g)


    clock = pygame.time.Clock()
    run = True

    #player = Player(WIDTH / 2, HEIGHT / 2, WHITE)
    pipes = [Pipe(WIDTH, -(HEIGHT / 2), GREEN)]
    score = 0
    while run:
        # caps the while loop at 60 iterations per second
        clock.tick(FPS)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                pygame.quit()
                quit()
            #if event.type == pygame.KEYUP and event.key == pygame.K_SPACE:
            #    player.jump()

        # get which pipe to look at (first or second assuming only 2 pipes at a time)
        pipe_ind = 0
        if len(players) > 0:
            if len(pipes) > 1 and players[0].x > pipes[0].x + PIPE_WIDTH:
                pipe_ind = 1
        else:
            # all players dead
            run = False
            break

        # giving input and receiving output
        for x, player in enumerate(players):
            player.update()
            ge[x].fitness += FIT_ADVANCE

            # here we only get the distance of between the Y
            output = nets[x].activate((player.y,
                                      abs(player.y - (pipes[pipe_ind].y + PIPE_HEIGHT)),
                                      abs(player.y - (pipes[pipe_ind].y + PIPE_HEIGHT + PIPE_GAP))))
            #output is a list (here we only have one so its len is 1)
            if output[0] > 0.5:
                player.jump()

        # DEAL WITH PIPES vvv
        rem = []
        to_add = False
        for pipe in pipes:
            to_add = False
            # instead of checking for one player check for all the players
            for x, player in enumerate(players):
                # also checks for world collisions
                collided, with_what = player.collided(pipe)
                if collided:
                    # WHEN BIRD DIES
                    # if player hits pip decrease fitness
                    if with_what == "world":
                        ge[x].fitness += FIT_COLLIDE
                    nets.pop(x)
                    ge.pop(x)
                    players.pop(x)

                if player.passed(pipe):
                    to_add = True

            if pipe.isOut:
                rem.append(pipe)
            pipe.update()

        if to_add:
            # add score maybe
            score += 1
            # add extra fitness to those that actually pass (still survived)
            for g in ge:
                g.fitness += FIT_PASS_PIPE
            pipes.append(Pipe(WIDTH, random.randint(PIPE_MIN_Y, PIPE_MAX_Y), GREEN))

        for r in rem:
            pipes.remove(r)
        # DEAL WITH PIPES ^^^

        draw_widow(players, pipes, score, len(players), GEN)


# boiler plate code for neat
def run(config_path):
    # this is neat.Header in the config file
    config = neat.config.Config(neat.DefaultGenome, neat.DefaultReproduction,
                                neat.DefaultSpeciesSet, neat.DefaultStagnation,
                                config_path)
    p = neat.Population(config)

    # optionnal print statistcs about generations
    p.add_reporter(neat.StdOutReporter(True))
    p.add_reporter(neat.StatisticsReporter())

    #fitness function We can use the main function
    winner = p.run(main, 50)

if __name__ == "__main__":
    #boiler plate code for neat
    # path to the current directory
    local_dir = os.path.dirname(__file__)
    config_path = os.path.join(local_dir, "config-feedforward.txt")
    run(config_path)
    # boiler plate code for neat
